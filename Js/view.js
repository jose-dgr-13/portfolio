var view = {
  getNavBar: function getNavBar() {
    var nav = document.createElement('nav');
    nav.id = 'navigator';
    nav.innerHTML = `
    <ul>
      <li><a href='home' name='home'>Home</a></li>
      <li><a href='about' name='about'>Know me</a></li>
      <li><a href='work' name='work'>What have I done?</a></li>
      <li><a href='skills' name='skills'>What else?</a></li>
      <li><a href='contact' name='contact'>Let's talk</a></li>
    </ul> 
    `;
    var that = this;
    var links = nav.querySelectorAll('a');
    links.forEach((element) => {
      element.addEventListener('click', (e) => {
        e.preventDefault();
        that.render(e.target.name);
      });
    });
    return nav;
  },

  getContactButton: function getContactButton() {
    var contact = document.createElement('div');
    contact.id = 'contact';
    contact.innerHTML = "<button name='name' class='cont' style='position:relative; top: 640px; right: -350px'>Let's Talk</button>";
    var cont = contact.querySelector('button');
    var that = this;
    cont.addEventListener('click', (e) => {
      e.preventDefault();
      that.render('contact');
    });
    return contact;
  },

  getBacktoWork: function getBacktoWork() {
    var backW = document.createElement('div');
    backW.id = 'contact';
    backW.innerHTML = "<button name='name' class='cont' style='position: absolute; top: 650px; left: 125px'>Show me more</button>";
    var that = this;
    var more = backW.querySelector('button');
    more.addEventListener('click', (e) => {
      e.preventDefault();
      that.render('work');
    })
    return backW;
  },

  getHome: function getHome() {
    var home = document.createElement('div');
    home.id = 'home';
    home.innerHTML = `
    <img class='window' src='./Source/Images/forest.jpg' width="400" height="400px"/>
    <img id='myImg' src='./Source/Images/cat.jpeg' width="415" height="415px"/>
    <div class="description">
    <h1> Welcome visitor... </h1>
    <p> We both are going to build experiences that enhance the user's vision of your product: <strong> It doesn't matter what is it, what really matters it's how they feel </strong> </p>
    </div>
    `;
    return home;
  },

  getAboutMe: function getAboutMe() {
    var about = document.createElement('div');
    about.id = 'about';
    about.innerHTML = `
    <h1> Who am I? </h1>
    <p>I am a student of INTERACTIVE MEDIA DESIGN at Icesi University. My areas of interest range from web development and UX / UI design, as well as illustration and project management skills (I'm currently working in the logistics area of Hoy es Diseño event) and innovation analysis. I participated in multiple conferences and design courses such as Hoy es Diseño and la Truca: animation festival, I have intervened in projects for the intervention of companies (Muntú early stimulation center and the restaurant "Platillos Voladores") and current member of the research group 418.</p>
    <div class='graphics'>
      <img src='Source/Images/cat.jpeg' width='388px' height='532px' />
      <div class='shapes'></div>
    </div>
    `;
    return about;
  },

  getDescription: function getDescription() {
    var desc = document.createElement('div');
    desc.id = 'description';
    desc.innerHTML = `
  <h1>What have I done?</h1>
  <p> Words are not enough to say something and as a designers we got the idea of multimedia supports as an enhancing tool for message. Here you are going to see what I have done through all my way. </p>
  `;
    var btn = desc.querySelector('button');
    return desc;
  },

  getPhotos: function getPhotos() {
    var photos = document.createElement('div');
    photos.id = 'photos';
    photos.innerHTML = `
    <a href='#'><div class='work'><img src="Source/images/capture/capture.jpg" alt="" /><h3>Capture de feeling / ILLUSTRATION </h3></div></a>
    <a href='#'><div class='work'><img src="Source/images/capture/capture.jpg" alt="" /><h3>Capture de feeling / ILLUSTRATION </h3></div></a>
    <a href='#'><div class='work'><img src="Source/images/capture/capture.jpg" alt="" /><h3>Capture de feeling / ILLUSTRATION </h3></div></a>
    <a href='#'><div class='work'><img src="Source/images/capture/capture.jpg" alt="" /><h3>Capture de feeling / ILLUSTRATION </h3></div></a>
    <a href='#'><div class='work'><img src="Source/images/capture/capture.jpg" alt="" /><h3>Capture de feeling / ILLUSTRATION </h3></div></a>
    `;
    var that = this;
    var links = photos.querySelectorAll('a');
    links[0].addEventListener('click', (e) => {
      e.preventDefault();
      that.render('feather');
    });
    return photos;
  },

  getDesign: function getDesign() {
    var lyric = `"'Cause you only need the light when it's burning low
    Only miss the sun when it starts to snow
    Only know you love her when you let her go
    Only know you've been high when you're feeling low
    Only hate the road when you're missing home
    Only know you love her when you let her go"`;
    var publish = document.createElement('div');
    publish.id = 'publish';
    publish.innerHTML = `
    <div class='graphics'>
    <!--<img src='Source/images/capture/forest3.png' width='323px' height='168px'/>-->
      <div style='position:fixed; width:250px; height:320px; background:white; top:225px; right:50px'></div>
    </div>
    <h1> CAPTURE THE FEELING </h1>
    <p class='descri'> "Capture the feeling" is a collection of different
    musicians and their songs whose lyrics have a 
    few words that make your feelings turn around 
    into a lovely experience </p>
    <img class='passenger' src='Source/images/capture/Passenger.png' />
    <div id='lyrics'><p>${lyric}</p></div>
    <div id='slider'>
      <img src='Source/images/capture/Alvaro.png' />
      <img src='Source/images/capture/Taburete.png' />
    </div>
    
    `;
    /*var sliderVar = publish.querySelectorAll('img');
      console.log(sliderVar[2]);
      setInterval((e) => {
        lyric = `Y me pongo a bailar y me empieza a crecer
        La insensatez, dadme de beber
        Porque estoy en motel, y me pongo a beber
        Y me pongo a bailar y me empieza a crecer
        La insensatez, dadme de beber
        Dadme de beber`;
      }, 3000);*/
    return publish;
  },

  getDesign: function getDesign() {
    var lyric = `"'Cause you only need the light when it's burning low
    Only miss the sun when it starts to snow
    Only know you love her when you let her go
    Only know you've been high when you're feeling low
    Only hate the road when you're missing home
    Only know you love her when you let her go"`;
    var publish = document.createElement('div');
    publish.id = 'publish';
    publish.innerHTML = `
    <div class='graphics'>
    <!--<img src='Source/images/capture/forest3.png' width='323px' height='168px'/>-->
      <div style='position:fixed; width:250px; height:320px; background:white; top:225px; right:50px'></div>
    </div>
    <h1> CAPTURE THE FEELING </h1>
    <p class='descri'> "Capture the feeling" is a collection of different
    musicians and their songs whose lyrics have a 
    few words that make your feelings turn around 
    into a lovely experience </p>
    <img class='passenger' src='Source/images/capture/Passenger.png' />
    <div id='lyrics'><p>${lyric}</p></div>
    <div id='slider'>
      <img src='Source/images/capture/Alvaro.png' />
      <img src='Source/images/capture/Taburete.png' />
    </div>
    
    `;
    /*var sliderVar = publish.querySelectorAll('img');
      console.log(sliderVar[2]);
      setInterval((e) => {
        lyric = `Y me pongo a bailar y me empieza a crecer
        La insensatez, dadme de beber
        Porque estoy en motel, y me pongo a beber
        Y me pongo a bailar y me empieza a crecer
        La insensatez, dadme de beber
        Dadme de beber`;
      }, 3000);*/
    return publish;
  },

  getFeather: function getFeather() {
    var publish = document.createElement('div');
    publish.id = 'publish';
    publish.innerHTML = `
    <div class='graphics'>
    <!--<img src='Source/images/capture/forest3.png' width='323px' height='168px'/>-->
      <div style='position:fixed; width:250px; height:320px; background:white; top:225px; right:50px'></div>
    </div>
    <h1> FEATHER </h1>
    <p class='descri'> FEATHER is a social red type application developed for women 
    where you can find and share awareness for health and 
    personal care.</p>
    <img class='passenger' src='Source/images/Feather/feathers.png' />
    <div id='lyrics'><p>This is how this application seeks an interaction connecting
    them together, sharing their opinions and the best advice to
    generate a collective mind in favor of mutual welfare.</p></div>
    <div id='slider'>
      <img src='Source/images/Feather/font1.png' />
      <img src='Source/images/Feather/font2.png' />
    </div>
    `;
    /*var sliderVar = publish.querySelectorAll('img');
      console.log(sliderVar[2]);
      setInterval((e) => {
        lyric = `Y me pongo a bailar y me empieza a crecer
        La insensatez, dadme de beber
        Porque estoy en motel, y me pongo a beber
        Y me pongo a bailar y me empieza a crecer
        La insensatez, dadme de beber
        Dadme de beber`;
      }, 3000);*/
    return publish;
  },

  getSkills: function getSkills() {
    var skills = document.createElement('div');
    skills.id = 'skill';
    skills.innerHTML = `
    <div class='circle'></div>
    <div class='descri des1'>
    <h1>Make everything better</h1>
    <p>"Capture the feeling" is a collection of different
    musicians and their songs whose lyrics have a 
    few words that make your feelings turn around 
    into a lovely experience</p>
    <button class='next' name='next1' />
    </div>
    <div class='descri des2'>
    <h1>The best you show of you, the more people that follows you</h1>
    <p>"Capture the feeling" is a collection of different
    musicians and their songs whose lyrics have a 
    few words that make your feelings turn around 
    into a lovely experience</p>
    <button class='next' name='next2' />
    </div>
    <div class='descri des3'>
    <h1>Let's make change</h1>
    <p>"Capture the feeling" is a collection of different
    musicians and their songs whose lyrics have a 
    few words that make your feelings turn around 
    into a lovely experience</p>
    <button class='next' name='again' />
    </div>
    `;
    var that = this;
    var buttons = skills.querySelectorAll('button');
    var descris = skills.getElementsByClassName('descri');
    buttons[0].addEventListener('click', (e) => {
      descris[0].style.opacity = '0';
      descris[0].style.top = '-610px';
      descris[1].style.top = '110px';
      descris[1].style.opacity = '1';
    });
    buttons[1].addEventListener('click', (e) => {
      descris[1].style.opacity = '0';
      descris[1].style.top = '-610px';
      descris[2].style.top = '110px';
      descris[2].style.opacity = '1';
    });
    buttons[2].addEventListener('click', (e) => {
      descris[2].style.opacity = '0';
      descris[2].style.top = '710px';
      descris[0].style.top = '110px';
      descris[0].style.opacity = '1';
    });
    for (i = 0; i < buttons.length; i++) {
      console.log(buttons[i].name);
      if (buttons[i].name == 'again') {
        buttons[i].style.backgroundImage = "url('./Source/Icons/replay.svg')";
      }
    }
    return skills;
  },

  getContact: function getContact() {
    var contact = document.createElement('div');
    contact.id = 'contact';
    contact.innerHTML = `
    <h1>Let's go for a coffee and talk...</h1>
    <h3>...That's how ideas are born</h3>
    <div class='links work'>
    <!--<p>Catch me on my professional network:</p>-->
    <h2><a target='_blank' href='https://www.behance.net/jose-dgr-13' ><img src='./Source/Icons/bh.svg' /></a> jose-dgr-13</h2>
    <h2><a target='_blank' href='https://www.linkedin.com/in/josedgr13' ><img src='./Source/Icons/in.svg' /></a> josedgr13</h2>
    </div>
    <div class='links social'>
    <!--<p> Catch me on my social network: </p>-->
    <h2><a target='_blank' href='https://www.facebook.com/jose.gomez.1327' ><img src='./Source/Icons/fb.svg' /></a> jose.gomez.1327</h2>
    <h2><a target='_blank' href='https://www.instagram.com/dr_feretto/' ><img src='./Source/Icons/ig.svg' /></a> dr_feretto</h2>
    <h2><a target='_blank' href='https://dribbble.com/jose-dgr-13' ><img src='./Source/Icons/db.svg' /></a> jose-dgr-13</h2>
    <!--<h2><a target='_blank' href='https://co.pinterest.com/josedgr13/' ><img src='./Source/Icons/pt.svg' /></a> josedgr13</h2>-->
    </div>
    `;

    return contact;
  },

  getPublish: function getPublish() {
    var gallery = document.createElement('div');
    gallery.id = 'Gallery';
    var descri = this.getDescription();
    var photos = this.getPhotos();

    gallery.appendChild(descri);
    gallery.appendChild(photos);
    setTimeout(() => {
      descri.style.left = '140px';
    }, 10);
    setTimeout(() => {
      photos.style.top = '60px';
      photos.style.overflowY = 'scroll';
    }, 1000);
    return gallery;
  },

  render: function (pagina) {
    var container = document.getElementById('container');
    container.innerHTML = ``;

    container.appendChild(this.getNavBar());
    pagina = 'feather';
    switch (pagina) {
      default: container.appendChild(this.getHome());
      container.appendChild(this.getContactButton());
      break;
      case 'about':
          container.appendChild(this.getAboutMe());
        container.appendChild(this.getContactButton());
        break;
      case 'work':
          container.appendChild(this.getPublish());
        break;
      case 'capture':
          container.appendChild(this.getDesign());
        container.appendChild(this.getBacktoWork());
        break;
      case 'feather':
          container.appendChild(this.getFeather());
        container.appendChild(this.getBacktoWork());
        break;
      case 'skills':
          container.appendChild(this.getSkills());
        container.appendChild(this.getContactButton());
        break;
      case 'contact':
          container.appendChild(this.getContact());
        break;
    }
  }

};