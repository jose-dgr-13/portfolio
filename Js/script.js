var descri = document.getElementById('description');
var btn = descri.querySelector('button');
var photos = document.getElementById('photos');
var bg = document.getElementsByClassName('bg')[0];

btn.addEventListener('click', (e) => {
  descri.style.marginLeft = '50px';
  descri.style.float = 'left';
  bg.style.opacity = '0.8';
  descri.style.color = 'white';
  setTimeout(()=>{
    photos.style.top = '5px';
    // document.body.style.overflow = 'scroll';
    photos.style.overflowY = 'scroll';
  },1000);

});
